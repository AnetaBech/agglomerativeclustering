# -*- coding: utf-8 -*-
"""
Created on Thu May 25 14:51:09 2017

@author: Dariusz Gala
"""
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt
from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import AgglomerativeClustering
from random import randint

#SETTINGS
np.set_printoptions(threshold=np.nan)
n_clusters = 2
centers = []
for i in range(n_clusters):
    centers.append([randint(0,9), randint(0,9)])

print(centers)
    
#GENEROWANIE DANYCH
#X = np.array([[1,1], [2,3], [5,4], [6,2], [1,9]])
X, y = make_blobs(n_samples = 50, centers = centers, cluster_std = 1.5)

#KLASTERYZACJA
clustering = AgglomerativeClustering(n_clusters = 2, linkage="ward")
clustering.fit(X)

labels = clustering.labels_
colors = ["r.","g.", "b.", "c.", "k.", "y.", "m."]
for i in range(len(X)):
    plt.text(X[i][0], X[i][1], str(i), fontdict={'weight': 'bold', 'size': 9})
    plt.plot(X[i][0], X[i][1], colors[labels[i]], markersize = 10)
plt.show()

#DENDROGRAM
Z = linkage(X, "ward")
plt.figure(figsize=(10, 5))
plt.title('Hierarchical Clustering Dendrogram')
plt.xlabel('sample index')
plt.ylabel('distance')
dendrogram(
    Z,
    leaf_rotation=90.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
)
plt.show()
